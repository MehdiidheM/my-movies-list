import * as axios from 'axios';

const apiMovie = axios.create({
    baseURL: 'https://api.themoviedb.org/4'
})

apiMovie.interceptors.request.use(req =>{
    req.headers['Authorization'] = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3OWZiNGY0ZmVmOWQ2Mjk1YzY1YjljMTk4MzM5OTZmNSIsInN1YiI6IjVjNDVmYjE3YzNhMzY4NDc3NzgzOTFlZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.EupqnKmPOFpF3OAlPs7JafJTiK7-nD6FomHG7dgRXFU'
    return req;

})

export default apiMovie

export const apiMovieMap = m =>({
    img:'https://image.tmdb.org/t/p/w500/' + m.poster_path,
    title:m.title,
    details:`${ m.release_date } | ${ m.vote_average }/10 (${ m.vote_count})`,
    description: m.overview
      })